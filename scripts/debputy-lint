#!/usr/bin/python3

import argparse
import os
import subprocess
import sys

from argparse import BooleanOptionalAction

def _deb_dir(path: str) -> str:
    while path and os.path.basename(path) != "debian":
        path = os.path.dirname(path)
    root_dir = os.path.dirname(path)
    if root_dir == "":
        root_dir = "."
    return root_dir


def main() -> None:
    parser = argparse.ArgumentParser()
    parser.add_argument('paths', nargs='*', help='Paths triggering the reformat')
    args = parser.parse_args()
    assert_has_debputy()
    unique_paths = {_deb_dir(p) for p in args.paths}
    cmd = ["debputy", "lint"]
    # TODO: Add auto-fix (but it must fail even if it corrects anything)
    cmd.append("--linter-exit-code")
    cmd.append("--no-warn-about-check-manifest")
    assert_has_debputy()
    unique_paths = {_deb_dir(p) for p in args.paths}
    for root_dir in unique_paths:
        try:
            subprocess.check_call(
                cmd,
                cwd=root_dir,
            )
        except subprocess.CalledProcessError:
            error(f"`debputy lint` reported errors for {root_dir!r}")
        if os.path.isfile(os.path.join(root_dir, "debian", "debputy.manifest")):
            try:
                subprocess.check_call(
                    ["debputy", "check-manifest"],
                    cwd=root_dir,
                )
            except subprocess.CalledProcessError:
                error(f"`debputy check-manifest` reported errors for {root_dir!r}")  


def assert_has_debputy() -> None:
    for p in os.environ["PATH"].split(":"):
        if os.access(os.path.join(p, "debputy"), os.X_OK):
            return
    error("Cannot find `debputy` in PATH")


def error(msg: str) -> None:
    sys.exit(f"debputy-lint [pre-commit]: {msg}")


if __name__ == "__main__":
    main()
