# debputy pre-commit hooks

This repository hosts hooks for `pre-commit` that uses `debputy`.

## Usage

You will need `debputy` plus some extra dependencies pre-installed prior to using these hooks.

     # apt satisfy 'dh-debputy (>= 0.1.30~), python3-lsprotocol'

Additionally, you will need `pre-commit` and install the hooks from this repo. The following hooks
are defined:

### Hook: debputy-lint

This hook will run `debputy lint` (and `debputy check-manifest` if relevant) to detect obvious or common
issues with the packaging.

This hook requires `debputy/0.1.30` or later.

### Hook: debputy-reformat

This hook will run `debputy reformat` to reformat or with `--no-auto-fix` detect but not correct formatting issues
in Debian files that `debputy` can reformat.

Note that packages must have a declared style, which can be done by having `X-Style: black` in `debian/control`
or having the style pre-declared in `debputy`'s built-in configuration.

This hook requires `debputy/0.1.30` or later.

### Sample file for debian/.pre-commit-config.yaml

```
files: 'debian/.*'
repos:
  - repo: https://salsa.debian.org/debian/debputy-pre-commit-hooks
    rev: main
    hooks:
      - id: debputy-lint
      - id: debputy-reformat
```

Then modify `.git/hooks/pre-commit` and add this argument to `ARGS=(...)`: `--config=debian/.pre-commit-config.yaml`
